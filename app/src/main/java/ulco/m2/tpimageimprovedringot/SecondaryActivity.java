package ulco.m2.tpimageimprovedringot;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SecondaryActivity extends AppCompatActivity {

    public final static String BUNDLE_PHONE = "ulco.m2.tpimageringot.BUNDLE_PHONE";
    public final static String BUNDLE_URL = "ulco.m2.tpimageringot.BUNDLE_URL";
    public final static String BUNDLE_LATITUDE = "ulco.m2.tpimageringot.BUNDLE_LATITUDE";
    public final static String BUNDLE_LONGITUDE = "ulco.m2.tpimageringot.BUNDLE_LONGITUDE";

    private EditText phone;
    private EditText url;
    private EditText latitude;
    private EditText longitude;
    private Button smsButton;
    private Button mmsButton;
    private Button callButton;
    private Button webButton;
    private Button mapButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secondary);
        findViews();
        createEventHandlers();
        retrieveDataFromBundle(savedInstanceState);
    }

    /** Permet d'initialiser les composants de l'activité destinés à être utilisé par la suite. */
    private void findViews(){
        this.phone = findViewById(R.id.editPhone);
        this.url = findViewById(R.id.editUrl);
        this.latitude = findViewById(R.id.editLatitude);
        this.longitude = findViewById(R.id.editLongitude);
        this.smsButton = findViewById(R.id.btnSms);
        this.mmsButton = findViewById(R.id.btnMms);
        this.callButton = findViewById(R.id.btnCall);
        this.webButton = findViewById(R.id.btnWeb);
        this.mapButton = findViewById(R.id.btnMap);
    }

    /** Permet d'associer à chaque bouton son listener. */
    private void createEventHandlers(){
        smsButton.setOnClickListener(v -> handleSms());
        mmsButton.setOnClickListener(v -> handleMms());
        callButton.setOnClickListener(v -> handleCall());
        webButton.setOnClickListener(v -> handleWeb());
        mapButton.setOnClickListener(v -> handleMap());
    }

    /** Gestion du sms. */
    private void handleSms(){
        if(checkPhone()){
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SENDTO);
            Uri sms = Uri.parse("sms:" + phone.getText());
            intent.setData(sms);
            if (intent.resolveActivity(getPackageManager()) != null)
                startActivity(intent);
        }
    }

    /** Gestion du mms. */
    private void handleMms(){
        if(checkPhone()){
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SENDTO);
            Uri mms = Uri.parse("mms:" + phone.getText());
            intent.setData(mms);
            if (intent.resolveActivity(getPackageManager()) != null)
                startActivity(intent);
        }
    }

    /** Gestion de l'appel. */
    private void handleCall(){
        if(checkPhone()){
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_DIAL);
            Uri tel = Uri.parse("tel:" + phone.getText());
            intent.setData(tel);
            if (intent.resolveActivity(getPackageManager()) != null)
                startActivity(intent);
        }
    }

    /** Gestion du lien web. */
    private void handleWeb(){
        if(checkUrl()){
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            Uri web = Uri.parse(url.getText().toString());
            intent.setData(web);
            if (intent.resolveActivity(getPackageManager()) != null)
                startActivity(intent);
        }
    }

    /** Gestion du lien géo. */
    private void handleMap(){
        checkLatitudeAndLongitude();
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri map = Uri.parse("geo:" + latitude.getText().toString() + "," + longitude.getText().toString());
        intent.setData(map);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    /** Vérification numéro. */
    private boolean checkPhone(){
        Pattern pattern = Pattern.compile("^\\d{10}$");
        Matcher matcher = pattern.matcher(phone.getText());
        return matcher.matches();
    }

    /** Vérification url web. */
    private boolean checkUrl() {
        Pattern pattern = Pattern.compile("\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");
        String strUrl = url.getText().toString();
        if(!(strUrl.startsWith("https://") || strUrl.startsWith("http://")))
            url.setText("https://" + url.getText());
        Matcher matcher = pattern.matcher(url.getText());
        return matcher.matches();
    }

    /** Vérification lien geo. */
    private void checkLatitudeAndLongitude(){
        if(latitude.getText().toString().isEmpty())
            latitude.setText("0.0");
        if(longitude.getText().toString().isEmpty())
            longitude.setText("0.0");
        double latitudeValue = Double.parseDouble(latitude.getText().toString());
        double longitudeValue = Double.parseDouble(longitude.getText().toString());
        if(latitudeValue > 90)
            latitude.setText("90");
        else if (latitudeValue < -90)
            latitude.setText("-90");
        if(longitudeValue > 180)
            longitude.setText("180");
        else if (longitudeValue < -180)
            longitude.setText("-180");
    }

    /** Permet de sauvegarder nos valeurs lors d'un changement d'orientation dans un bundle.
     *
     * @param outState Le bundle stockant nos valeurs à sauvegarder.
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(BUNDLE_PHONE, phone.getText().toString());
        outState.putString(BUNDLE_URL, url.getText().toString());
        outState.putString(BUNDLE_LATITUDE, latitude.getText().toString());
        outState.putString(BUNDLE_LONGITUDE, longitude.getText().toString());
        super.onSaveInstanceState(outState);
    }

    /** Permet de restaurer nos valeurs lors d'un changement d'orientation à partir du bundle.
     *
     * @param savedInstanceState Le bundle stockant nos valeurs à restaurer.
     */
    private void retrieveDataFromBundle(Bundle savedInstanceState){
        if (savedInstanceState != null) {
            phone.setText(savedInstanceState.getString(BUNDLE_PHONE));
            url.setText(savedInstanceState.getString(BUNDLE_URL));
            latitude.setText(savedInstanceState.getString(BUNDLE_LATITUDE));
            longitude.setText(savedInstanceState.getString(BUNDLE_LONGITUDE));
        }
    }
}