package ulco.m2.tpimageimprovedringot;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    public final static int IMAGE_REQUEST = 1;
    public final static String BUNDLE_LINK = "ulco.m2.tpimageringot.BUNDLE_LINK";

    private TextView uri;
    private Button loadButton;
    private Button goToSecondaryActivity;
    private ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Ajout d'un icone dans la barre d'application.
        Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.icon_foreground);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        findViews();
        createEventHandlers();
        retrieveDataFromBundle(savedInstanceState);
    }

    /** Permet d'initialiser les composants de l'activité destinés à être utilisé par la suite. */
    private void findViews(){
        this.uri = findViewById(R.id.uri);
        this.loadButton = findViewById(R.id.load);
        this.goToSecondaryActivity = findViewById(R.id.goToSecondaryActivity);
        this.image = findViewById(R.id.image);
        registerForContextMenu(image);
    }

    /** Permet d'associer à chaque bouton son listener. */
    private void createEventHandlers(){
        loadButton.setOnClickListener(v -> handleLoad());
        goToSecondaryActivity.setOnClickListener(v -> goToSecondaryActivity());
    }

    /** Passage dans l'activité secondaire. */
    private void goToSecondaryActivity(){
        Intent intent = new Intent(this, ulco.m2.tpimageimprovedringot.SecondaryActivity.class);
        startActivity(intent);
    }

    /** Gestion du chargement d'image. */
    private void handleLoad(){
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, IMAGE_REQUEST);
        }
    }

    /** Gestion du miroir horizontal. */
    private void handleHorizontalReverse(){
        if(image.getDrawable() != null){
            Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
            int height = bitmap.getHeight();
            int width = bitmap.getWidth();
            for(int y = 0; y < height ; y++){
                for(int x = 0 ; x < width / 2 ; x++){
                    int p = bitmap.getPixel(x, y);
                    bitmap.setPixel(x, y, bitmap.getPixel(width - x - 1, y));
                    bitmap.setPixel(width - x - 1, y, p);
                }
            }
            image.setImageBitmap(bitmap);
        }
    }

    /** Gestion du miroir vertical. */
    private void handleVerticalReverse(){
        if(image.getDrawable() != null){
            Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
            int height = bitmap.getHeight();
            int width = bitmap.getWidth();
            for(int y = 0; y < height / 2 ; y++){
                for(int x = 0 ; x < width ; x++){
                    int p = bitmap.getPixel(x, y);
                    bitmap.setPixel(x, y, bitmap.getPixel(x, height - y - 1));
                    bitmap.setPixel(x, height - y - 1, p);
                }
            }
            image.setImageBitmap(bitmap);
        }
    }

    /** Gestion de l'inversement de couleurs. */
    private void handleInverseColor(){
        if(image.getDrawable() != null){
            Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    bitmap.setPixel(i, j, bitmap.getPixel(i, j) ^ 0x00ffffff);
                }
            }
            image.setImageBitmap(bitmap);
        }
    }

    /** Gestion du passage en niveau de gris. */
    private void handleGreyLevel(){
        if(image.getDrawable() != null){
            Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    int px = bitmap.getPixel(i, j);
                    int avg = (Color.red(px) + Color.green(px) + Color.blue(px)) / 3;
                    bitmap.setPixel(i, j, Color.argb(Color.alpha(px), avg, avg, avg));
                }
            }
            image.setImageBitmap(bitmap);
        }
    }

    /** Gestion du passage en niveau de gris 2. */
    private void handleGreyLevel2(){
        if(image.getDrawable() != null){
            Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    int px = bitmap.getPixel(i, j);
                    int avg = (Math.max(Color.red(px), Math.max(Color.green(px), Color.blue(px))) + Math.min(Color.red(px), Math.min(Color.green(px), Color.blue(px)))) / 2;
                    bitmap.setPixel(i, j, Color.argb(Color.alpha(px), avg, avg, avg));
                }
            }
            image.setImageBitmap(bitmap);
        }
    }

    /** Gestion du passage en niveau de gris 3. */
    private void handleGreyLevel3(){
        if(image.getDrawable() != null){
            Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    int px = bitmap.getPixel(i, j);
                    int avg = (int) (0.21*Color.red(px) + 0.72*Color.green(px) + 0.07*Color.blue(px));
                    bitmap.setPixel(i, j, Color.argb(Color.alpha(px), avg, avg, avg));
                }
            }
            image.setImageBitmap(bitmap);
        }
    }

    /** Gestion de la restauration d'image. */
    private void handleRestore(){
        image.setImageBitmap(getImage(uri.getText().toString()));
    }

    /** Gestion de la rotation d'image dans le sens horaire. */
    private void handleClockwiseRotation(){
        if(image.getDrawable() != null){
            Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            Bitmap newBitmap = Bitmap.createBitmap(height, width, bitmap.getConfig());
            for (int i = 0; i <width; i++) {
                for (int j = 0; j < height; j++) {
                    int p = bitmap.getPixel(i, height - j - 1);
                    newBitmap.setPixel(j, i, p);
                }
            }
            image.setImageBitmap(newBitmap);
        }
    }

    /** Gestion de la rotation d'image dans le sens anti-horaire. */
    private void handleAntiClockwiseRotation(){
        if(image.getDrawable() != null){
            Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            Bitmap newBitmap = Bitmap.createBitmap(height, width, bitmap.getConfig());
            for (int i = 0; i <width; i++) {
                for (int j = 0; j < height; j++) {
                    int p = bitmap.getPixel(width - i - 1, j);
                    newBitmap.setPixel(j, i, p);
                }
            }
            image.setImageBitmap(newBitmap);
        }
    }

    /** Gestion du retour d'activité. */
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == IMAGE_REQUEST && resultCode == RESULT_OK){
            uri.setText(data.getData().toString());
            image.setImageBitmap(getImage(uri.getText().toString()));
        }
    }

    /** Récupère une image à partir de son uri. */
    private Bitmap getImage(String uri){
        try {
            BitmapFactory.Options option = new BitmapFactory.Options();
            option.inMutable = true;
            return BitmapFactory.decodeStream(getContentResolver().openInputStream(Uri.parse(uri)), null, option);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    /** Permet de sauvegarder nos valeurs lors d'un changement d'orientation dans un bundle.
     *
     * @param outState Le bundle stockant nos valeurs à sauvegarder.
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(BUNDLE_LINK, uri.getText().toString());
        super.onSaveInstanceState(outState);
    }

    /** Permet de restaurer nos valeurs lors d'un changement d'orientation à partir du bundle.
     *
     * @param savedInstanceState Le bundle stockant nos valeurs à restaurer.
     */
    private void retrieveDataFromBundle(Bundle savedInstanceState){
        if (savedInstanceState != null) {
            uri.setText(savedInstanceState.getString(BUNDLE_LINK));
            try {
                if(uri.getText() != null)
                    image.setImageBitmap(MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.parse(uri.getText().toString())));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.horizontalMirror:
                handleHorizontalReverse();
                return true;
            case R.id.verticalMirror:
                handleVerticalReverse();
                return true;
            case R.id.restore:
                handleRestore();
                return true;
            case R.id.clockwiseRotation:
                handleClockwiseRotation();
                return true;
            case R.id.antiClockwiseRotation:
                handleAntiClockwiseRotation();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.image_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.inverseColor:
                handleInverseColor();
                return true;
            case R.id.greyLevel:
                handleGreyLevel();
                return true;
            case R.id.greyLevel2:
                handleGreyLevel2();
                return true;
            case R.id.greyLevel3:
                handleGreyLevel3();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

}