# TPimageRingot

Projet - TP : Application Android (java)

Gestion d'image, permissions et intent implicites.
## Auteur

- [Ringot Arthur](https://gitlab.com/aringot1612)

## Aperçus :

### Page principale (portrait)

<br>![Aperçu : Page principale (portrait)](images/main_portrait.png)

### Page principale (paysage)

<br>![Aperçu : Page principale (paysage)](images/main_landscape.png)

### Page secondaire (portrait)

<br>![Aperçu : Page secondaire (portrait)](images/optional_portrait.png)

### Page secondaire (paysage)

<br>![Aperçu : Page secondaire (paysage)](images/optional_landscape.png)

### Miroir horizontal

<br>![Aperçu : Miroir horizontal](images/horizontalMirror.png)

### Miroir vertical

<br>![Aperçu : Miroir vertical](images/verticalMirror.png)

### Inversement des couleurs

<br>![Aperçu : Inversement des couleurs](images/colorInversed.png)

### Niveau de gris 1

<br>![Aperçu : Niveau de gris 1](images/grey_1.png)

### Niveau de gris 2

<br>![Aperçu : Niveau de gris 2](images/grey_2.png)

### Niveau de gris 3

<br>![Aperçu : Niveau de gris 3](images/grey_3.png)

### Rotation à 90 degrés de l’image dans le sens horaire

<br>![Aperçu : Rotation horaire](images/rotateClockWise.png)

### Rotation à 90 degrés de l’image dans le sens anti-horaire

<br>![Aperçu : Rotation anti-horaire](images/rotateAntiClockWise.png)